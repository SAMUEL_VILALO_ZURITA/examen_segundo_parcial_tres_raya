let juego = document.getElementById('ttt'), //canvas
    ctx = juego.getContext('2d'),
    msg = document.getElementById('message'),
    cellSize = 100,
    map = [//matris
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
    ],
    //winPatterns=patroness
    patrones = [
        0b111000000, 0b000111000, 0b000000111, // fila
        0b100100100, 0b010010010, 0b001001001, // Columna
        0b100010001, 0b001010100, // Diagonal
    ],
    BLANK = 0, X = 1, O = -1,
    mouse = {
        x: -1,
        y: -1,
    },
    currentPlayer = X,
    gameOver = false,
    winCells = [];

//tamaño de la matris
juego.width = juego.height = 3 * cellSize;


juego.addEventListener('mousemove', function (e) {
    let x = e.pageX - juego.offsetLeft,
        y = e.pageY - juego.offsetTop;

    mouse.x = x;
    mouse.y = y;
});

juego.addEventListener('click', function (e) {
    jugar(getCellByCoords(mouse.x, mouse.y));
});

turno_jugador();

function turno_jugador () {
    msg.textContent = ((currentPlayer == X)? 'X': 'O') + '\'turno del jugador';
}

function jugar (cell) {
    if (gameOver) return;

    if (map[cell] != BLANK) {
        msg.textContent = 'posicion ya ocupada.';
        return;
    }

    map[cell] = currentPlayer;

    let winCheck = cheque_ganar(currentPlayer);

    if (winCheck != 0) {
        gameOver = true;
        msg.textContent = ((currentPlayer == X)? 'X': 'O') + ' Ganador';

        let bit = 1;
        for (let i = map.length - 1; i >= 0; i--) {
            if ((bit & winCheck) === bit) {
                winCells.push(i);
            }
            bit <<= 1;
        }
        return;
    } else if (map.indexOf(BLANK) == -1) {
        gameOver = true;
        msg.textContent = 'Empate';
        return;
    }

    currentPlayer *= -1;

    turno_jugador();
}

function cheque_ganar (player) {
    let playerMapBitMask = 0;
    for (let i = 0; i < map.length; i++) {
        playerMapBitMask <<= 1;
        if (map[i] == player)
            playerMapBitMask += 1;
    }

    for (let i = 0; i < patrones.length; i++) {
        if ((playerMapBitMask & patrones[i]) == patrones[i]) {
            return patrones[i];
        }
    }

    return 0;
}

function draw () {
    ctx.clearRect(0, 0, juego.width, juego.height);
    resaltar_mouse();
    drawWinHighlight();
    drawBoard();
    fillBoard();

    function resaltar_mouse () {
        if (gameOver) return;

        let cellNum = getCellByCoords(mouse.x, mouse.y),
            cellCoords = getCellCoords(cellNum);
    }
   //mostrar en pantalla toda la figura
   function drawWinHighlight () {
        if (gameOver) {
            ctx.fillStyle = 'rgb(18, 102, 18)';
            winCells.forEach(function (i) {
                let cellCoords = getCellCoords(i);
                ctx.fillRect(cellCoords.x, cellCoords.y, cellSize, cellSize);
            });
        }
    }

    function drawBoard () {
        ctx.strokeStyle = 'white';
        ctx.lineWidth = 10;

        ctx.beginPath();
        ctx.moveTo(cellSize, 0);
        ctx.lineTo(cellSize, juego.height);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(cellSize * 2, 0);
        ctx.lineTo(cellSize * 2, juego.height);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(0, cellSize);
        ctx.lineTo(juego.width, cellSize);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(0, cellSize * 2);
        ctx.lineTo(juego.width, cellSize * 2);
        ctx.stroke();
    }

    function fillBoard () {
        ctx.strokeStyle = 'white';
        ctx.lineWidth = 5;
        for (let i = 0; i < map.length; i++) {
            let coords = getCellCoords(i);

            ctx.save();
            ctx.translate(coords.x + cellSize / 2, coords.y + cellSize / 2);
            if (map[i] == X) {
                drawX();
            } else if (map[i] == O) {
                drawO();
            }
            ctx.restore();
        }
    }

    function drawX () {
        ctx.beginPath();
        ctx.moveTo(-cellSize / 3, -cellSize / 3);
        ctx.lineTo(cellSize / 3, cellSize / 3);
        ctx.moveTo(cellSize / 3, -cellSize / 3);
        ctx.lineTo(-cellSize / 3, cellSize / 3);
        ctx.stroke();
    }

    function drawO () {
        ctx.beginPath();
        ctx.arc(0, 0, cellSize / 3, 0, Math.PI * 2);
        ctx.stroke();
    }

    requestAnimationFrame(draw);//solicitar cuadro de animacion
}

function getCellCoords (cell) {
    let x = (cell % 3) * cellSize,
        y = Math.floor(cell / 3) * cellSize;
    
    return {
        'x': x,
        'y': y,
    };
}

function getCellByCoords (x, y) {
    return (Math.floor(x / cellSize) % 3) + Math.floor(y / cellSize) * 3;
}

draw();